/*
 * Codegen.h - Code generation tools for turtle compiler
 * Tim Sergeant (u5006960), 2013
 */
#ifndef CODEGEN_H
#define CODEGEN_H

#define OP_HALT	0x0000
#define OP_UP  	0x0A00
#define OP_DOWN	0x0C00
#define OP_MOVE 0x0E00
#define OP_ADD  0x1000
#define OP_SUB  0x1200
#define OP_NEG  0x2200
#define OP_MUL  0x1400
#define OP_TEST 0x1600
#define OP_RTS  0x2800


#define OP_LOAD  0x0600
#define OP_STORE 0x0400
#define OP_READ  0x0200

#define OP_JSR   0x6800
#define OP_JUMP  0x7000
#define OP_JEQ   0x7200
#define OP_JLT   0x7400
#define OP_LOADI 0x5600
#define OP_POP   0x5E00

typedef unsigned short addr;

/*
 * Returns the next available address in the code block
 */
addr getAddr();

/*
 * Adds a single word instruction to the code block
 * Returns the address of the instruction
 */
addr emit(unsigned short instruction);

/*
 * Adds a single word instruction with offset to the code block
 */
addr emitOffset(unsigned short instruction, int fp, signed char offset);

/*
 * Emits a two-word instruction. Returns the address of the first word
 */
addr emitTwo(unsigned short instruction, unsigned short argument);

/*
 * Backpatches a new value into the second word of a two-word instruction.
 *
 * Note that the address of the first word should be given. Ie,
 *
 * addr a = emitTwo(OP_JUMP, 0);
 * backpatch(a, f->address);
 *
 * even though it's the second word we want to backpatch.
 */
void backpatch(addr address, addr jumpAddress);

/*
 * Dumps the codeblock to the given file
 */
void dumpCode(char *filename);

#endif
