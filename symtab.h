/*
 * Symtab.h - Symbol table for turtle compiler
 * Tim Sergeant (u5006960), 2013
 */
#ifndef SYMTAB_H
#define SYMTAB_H

#define FUNCTAB_SIZE 100
#define VARTAB_SIZE 1000

/*
 * Representation of a function definition for use in the symbol table
 */
typedef struct {
    char *ident; //Function name
    int address; //Location in generated code
    char nparam; //Number of parameters to the function
    char nvar;   //Number of local variables -- does not include arguments
} sym_func;

/*
 * Adds a new function to the symbol table. 
 *
 * Returns the function object if it was successfully added,
 * or NULL if a function with that name already exists
 */
sym_func *addFunc(char *ident, int address);

/*
 * Searches for a function with the given name in the symbol table
 * Returns NULL if it could not be found.
 */
sym_func *getFunc(char *ident);

typedef struct {
    char *ident;      //Variable name
    sym_func *scope;  //Function containing the variable, or NULL if global
                      //obviously, we would need a much more complex concept
                      //of scope if the language had more than two levels
                      //of declarations. 
    signed char addr; //Location of the var, relative to the scope.
} sym_var;

/*
 * Adds a new variable to the symbol table
 *
 * Returns the variable object, or NULL if a var with that name
 * already exists in the same scope.
 */
sym_var *addVar(char *ident, sym_func *scope, signed char addr);

/*
 * Searches through the symbol table for a variable.
 *
 * If scope is not-NULL, local variables will be given preference over globals
 * Returns NULL if the var could not be found
 */
sym_var *getVar(char *ident, sym_func *scope);

/*
 * Similar to getVar, but does not default to global scope if the variable
 * could not be found locally.
 */
sym_var *getVarSameScope(char *ident, sym_func *scope);

/*
 * Gets the next available variable address within a given scope. 
 */
signed char getVarAddr(sym_func *scope);

/*
 * Prints the function and variable symbol tables to stdout.
 */
void printTable();

#endif
