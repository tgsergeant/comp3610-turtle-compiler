%{
    // vim:sw=4:ts=4:et:
    /*
     * Flex specification for turtle compiler
     * By Tim Sergeant (u5006960), 2013
     */

    #include <stdio.h>
    #include <string.h>
    #include "turtle.tab.h"

    //Take care of line numbers
    #define YY_USER_ACTION {                                \
        yylloc.first_line = yylloc.last_line = yylineno;    \
    }
%}

%option yylineno

DIGIT [0-9]
ALPHA [A-Za-z]


%%

\/\/.*\n  //Comment -- do nothing!

turtle  return TURTLE;
up      return UP;
down    return DOWN;
moveto  return MOVETO;
var     return VAR;
fun     return FUN;
read    return READ;
if      return IF;
else    return ELSE;
while   return WHILE;
return  return RETURN;

\+      return PLUS;
\-      return MINUS;
\*      return MULT;
\(      return LPAREN;
\)      return RPAREN;
\{      return LBRACE;
\}      return RBRACE;
\,      return COMMA;
\<       return LT;
==      return EQ;
=       return GETS;

{DIGIT}+ {
            yylval.ival = atoi(yytext);
            return NUM;
        }

{ALPHA}[A-Za-z0-9_]*   {
                            yylval.id = strdup(yytext);
                            return IDENT;
                        }
[ \n\t\r]+  /*ignore*/
<<eof>>     { yyterminate(); }
