#!/bin/bash
echo "---Turtle---"
./turtlec $1 out/out.p -v
echo "---Pdplot---"
if (( $# == 2))
then
	echo "Data file specified"
	tools/PDPlot-C/pdplot out/out.p out/out.png 1000 1000 < $2
else
	tools/PDPlot-C/pdplot out/out.p out/out.png 1000 1000
fi
echo "---DisASM---"
tools/DisASM/DisASM out/out.p out/out.asm
cat out/out.asm
