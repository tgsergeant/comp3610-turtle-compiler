COMP3610 Assignment 2: Turtle Compiler
===

By Timothy Sergeant (u5006960), 2013.

Building
---

Since regenerating flex and bison is such a tedious process, I've been using a
simple script (./build.sh) which does the process for me - just running flex,
bison and then GCC.

Everything compiles and runs successfully with Bison 3.0, Flex 2.5.37 and GCC
4.8.1. Earlier versions of Bison almost certainly won't work.

Running
---

Turtlec has a simple argument specification:

    ./turtlec in.t out.p [-v]

The optional -v flag enables output of the function and variable symbol tables
after parsing is complete. This is moderately useful for debugging.

To simplify the process of running turtlec, PDPPlot and DisASM, a second script
is provided:

    ./plot.sh in.t [data.d]

This will compile the turtle program (to out/out.p), run that through PDPPlot-C
(to out/out.png), taking data from data.d or stdin if no file is given, and then
disassemble out/out.p to out/out.asm, printing this file to stdout. This script
assumes that the turtletools.tgz archive has been extracted into the same
directory as the code.

Code Overview
---

Code has been split up into a handful of modules:

* Turtle.y: Bison specification.
  Where semantic actions were needed in the middle of a production, I generally
  refactor the grammar and split the production, unless the action is fairly
  trivial

* Turtle.l: Flex specication

* symtab.h/c: Provides symbol tables for variables and functions

* codegen.h/c: Provides functions for emitting functions to the main code block

* turtlec.c: Main function

A number of test cases have been included in the mytests/ folder. See
mytests/tests.md for additional details.

Language design decisions
---

* You may not declare to functions with the same name but different numbers of
  arguments

* Functions and variables have completely different symbol tables. As a result,
  you may have functions and variables which share a name.

* At the moment, a handful of corner cases are not considered -- for example,
  functions with too many parameters to be handled by PDPlot or running out
  of space for code. In reality, these are so unlikely to happen that leaving
  them unhandled is not a big issue.
