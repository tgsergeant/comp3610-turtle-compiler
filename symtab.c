/*
 * Symtab.c - Symbol table for turtle compiler
 * Tim Sergeant (u5006960), 2013
 */
#include "symtab.h"
#include <stdio.h>
#include <string.h>

int findex = 0;
sym_func funcTable[FUNCTAB_SIZE];

sym_func *addFunc(char *ident, int address) {
    if(getFunc(ident)) {
	return NULL; //Return null if the function already exists
    }

    sym_func *entry = &funcTable[findex];
    entry->ident = ident;
    entry->address = address;
    entry->nparam = 0;
    entry->nvar = 0;

    findex++;

    return entry;
}

sym_func *getFunc(char *ident) {
    int i;
    for(i = 0; i < findex; i++) {
	if(strcmp(ident, funcTable[i].ident) == 0) {
	    return &funcTable[i];
	}
    }
    return NULL;
}

sym_var varTable[VARTAB_SIZE];
int vindex = 0;

sym_var *addVar(char *ident, sym_func *scope, signed char addr) {
    if(getVarSameScope(ident, scope) != NULL) {
	return NULL;
    }
    sym_var *entry = &varTable[vindex];
    entry->ident = ident;
    entry->scope = scope;
    entry->addr = addr;

    vindex++;

    return entry;
}

sym_var *getVar(char *ident, sym_func *scope) {
    sym_var *globalVar = NULL;
    int i;
    
    for(i = 0; i < vindex; i++) {
	if(strcmp(ident, varTable[i].ident) == 0) {
	    if(varTable[i].scope == NULL) {
		globalVar = &varTable[i];
		if(scope == NULL) {
		    return globalVar;
		}
	    }
	    else if(scope != NULL && 
		    scope == varTable[i].scope) {
		return &varTable[i];
	    }
	}
    }

    return globalVar;
}

sym_var *getVarSameScope(char * ident, sym_func *scope) {
    int i;

    for(i = 0; i < vindex; i++) {
	if(strcmp(ident, varTable[i].ident) == 0) {
	    //Identifiers are the same, check the scope
	    if(scope == varTable[i].scope) {
		return &varTable[i];
	    }
	    /*if(scope == NULL && varTable[i].scope == NULL) {*/
		/*return &varTable[i];*/
	    /*}*/
	    /*if(scope != NULL && */
	       /*scop) {*/
		/*return &varTable[i];*/
	    /*}*/
	}
    }

    return NULL;
}

signed char globalVarCount = 0;

signed char getVarAddr(sym_func *scope) {
    if(scope == NULL) {
	return ++globalVarCount;
    }
    else {
	return ++(scope->nvar);
    }
}

void printTable() {
    int i;
    for(i = 0; i < findex; i++) {
	printf("F-%d: %s (At %d with %d params)\n", i, funcTable[i].ident, funcTable[i].address, funcTable[i].nparam);
    }
    sym_var v;
    for(i = 0; i < vindex; i++) {
	v = varTable[i];
	printf("V-%d: %s (In %s scope, addr %d)\n", i, v.ident, 
		v.scope == NULL ? "Global" : v.scope->ident, v.addr);
    }
}
