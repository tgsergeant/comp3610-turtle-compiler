/*
 * Turtlec.c - Main method for turtle compiler
 * By Tim Sergeant (u5006960), 2013
 */
#include "turtle.tab.h"
#include "codegen.h"
#include "symtab.h"

#include <stdio.h>
#include <string.h>

extern FILE *yyin;

int main(int argc, char *argv[]) {
    if(argc < 3) {
	printf("Usage: ./turtlec input.t output.p [-v]\n");
	return 1;
    }

    FILE *infile = fopen(argv[1], "r");
    if(!infile) {
	printf("Could not open input file\n");
	return 1;
    }
    yyin = infile;

    int result = yyparse();    
    if(result == 0) {
	if(argc == 4 && strcmp("-v", argv[3]) == 0) {
	    printTable();
	}
	dumpCode(argv[2]);

	return 0;
    }
    return 1;
}
