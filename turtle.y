
%{
    // vim:sw=4:ts=4:et:
    /*
     * Bison specification for turtle compiler
     * By Tim Sergeant (u5006960), 2013
     */
    #include <stdio.h>

    #include "codegen.h"
    #include "symtab.h"

    void yyerror(const char *s);

    /*
     * Emit the code to call a function, assuming that the return
     * value and arguments are already on the stack. If no functin
     * with this name is known, defer until later (callTable).
     * 
     * Returns 1 on success, 0 on failure (so we can YYERROR). 
     */
    int callFunc(char *ident, int numArgs);
    
    /*
     * Print out a yyerror for an undefined variable, since this
     * appears in a couple of places.
     */
    void errorUndefinedVar(char *ident);

    //The current function the parser is in, or NULL.
    //This was the easiest way to deal with things like
    //return statements
    sym_func *scope = NULL;
    
    //Used to store unresolved function calls to resolve
    //once parsing is complete
    typedef struct {
        char *name;
        int nparam;
        addr jumploc;
        int line;
    } func_call;


    int fcallindex = 0;
    func_call callTable[200];
%}

%define parse.error verbose
%locations

%code requires {
    //Make sure that symtab definitions are available for %union
    #include "symtab.h"
    typedef unsigned short addr;
}

%union {
    char *id;
    int ival;
    addr address; 
    sym_func *func;
}

%token <id> IDENT
%token <ival> NUM

%token TURTLE
%token UP
%token DOWN
%token MOVETO
%token VAR
%token FUN
%token READ
%token IF
%token ELSE
%token WHILE
%token RETURN

%token PLUS
%token MINUS
%token MULT
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token COMMA
%token LT
%token EQ
%token GETS

%type <func> func_ident
%type <ival> opt_param_list
%type <ival> param_list
%type <ival> opt_arg_list
%type <ival> arg_list

%type <address> global_decl_list
%type <address> comp
%type <address> whilepart
%type <address> elsepart

/* Trickery with precedence to make hanging else statements work properly
 * -- else has lower precedence than statements without an else
 */
%precedence THENPART
%precedence ELSE


%start program


%%

program : TURTLE IDENT global_decl_list func_list 
        {
            //Once we know where the function declarations end, backpatch the jump
            //we placed after the variable declarations
            backpatch($3, getAddr());
        } 
        comp_stmt 
        { 
            emit(OP_HALT);

            //Check up on all of the unknown function calls
            int i;
            for(i = 0; i < fcallindex; i++) {
                func_call call = callTable[i];
                sym_func *f = getFunc(call.name);
                if(f == NULL) {
                    //No such function
                    printf("Error: No declaration found for function %s, implicit declaration at line %d\n", call.name, call.line);
                    YYERROR;
                } else if(f->nparam != call.nparam) {
                    //Call with incorrect params
                    printf("Error: Expected %d parameters for function %s, received %d when implicitly declared at line %d\n", 
                            f->nparam, call.name, call.nparam, call.line);
                    YYERROR;
                }
                else {
                    //Successfully matched, backpatch the JSR operation
                    backpatch(call.jumploc, f->address);
                }
            }
        }
	    ;


/* VARIABLE DECLARATIONS */

global_decl_list : decl_list { 
                 //Emit a jump and save the address so we can patch it later
                 $$ = emitTwo(OP_JUMP, 0); 
                }

decl_list : decl decl_list 
          | /* empty */ 
          ;

decl : VAR IDENT opt_gets {
        //Declare a new variable within the scope
        signed char addr = getVarAddr(scope);
        sym_var *v = addVar($2, scope, addr);
        
        if(v == NULL) {
            char msg[100];
            sprintf(msg, "Repeated variable declaration in scope: %s", $2);
            yyerror(msg);
            YYERROR;
        }
     }
     ; 

opt_gets : GETS expr /* result ends up on top of the stack */
         | /* empty */ { emitTwo(OP_LOADI, 0); }


/* FUNCTION DEFINITIONS */

func_list : func_dec func_list
          | /* empty */
          ;

func_dec : func_ident LPAREN opt_param_list 
            {
                $1->nparam = $3;
            }
            RPAREN decl_list comp_stmt
           {
                scope = NULL;
                emit(OP_RTS);
            }
         ;

func_ident : FUN IDENT {
                sym_func *f = addFunc($2, getAddr());
                if(!f) {
                    char msg[100];
                    sprintf(msg, "Repeated definition of function: %s", $2);
                    yyerror(msg);
                    YYERROR;
                }
                scope = f;
                $$ = f;
            }

opt_param_list : IDENT param_list 
               { 
                //Return value is the number of parameters seen in the list so far
                //-2 - $2 gives the position in the stack we want to use for
                //this particular parameter. 
                signed char address = -2 - $2;
                addVar($1, scope, address);
                $$ = $2 + 1;
                }
               | /* empty */ { $$ = 0; }
               ;

param_list : COMMA IDENT param_list
           { 
                $$ = $3 + 1; 
                signed char address = -2 - $3;
                addVar($2, scope, address);
                $$ = $3 + 1;
            }
           | /* empty - base case */ { $$ = 0; }
           ;


/* STATEMENTS */

comp_stmt : LBRACE stmt_list RBRACE
          ;

stmt_list : stmt stmt_list
          | stmt
          ;

stmt : UP { emit(OP_UP);}
     | DOWN { emit(OP_DOWN); }
     | MOVETO LPAREN expr COMMA expr RPAREN { emit(OP_MOVE);}
     | READ LPAREN IDENT RPAREN 
        { 
            sym_var *v = getVar($3, scope);
            if(v == NULL) {
                errorUndefinedVar($3);
                YYERROR;
            }
            emitOffset(OP_READ, v->scope != NULL ? 1 : 0, v->addr);
        }
     | IDENT GETS expr 
        { 
            sym_var *v = getVar($1, scope);
            if(v == NULL) {
                errorUndefinedVar($1);
                YYERROR;
            }
            else {
                emitOffset(OP_STORE, v->scope != NULL ? 1 : 0, v->addr);
            }
        }
     | IF LPAREN comp RPAREN stmt 
        { 
            backpatch($3, getAddr());
        } %prec THENPART

     | IF LPAREN comp RPAREN stmt elsepart stmt
        {
            //Elsepart emits a jump before the else statement is processed
            //Patch this to go to the end of the statement
            backpatch($6, getAddr());
            //Patch the false jump in cond to be right at the start of else
            //(Right after the previous jump)
            backpatch($3, $6+2);
        }

     | whilepart LPAREN comp RPAREN stmt
        { 
            //Jump back to the start, and then backpatch the false jump
            emitTwo(OP_JUMP, $1);
            backpatch($3, getAddr());
        }
     | RETURN expr 
        { 
            if(scope == NULL) {
                char msg[100];
                sprintf(msg, "Return statement outside of function");
                yyerror(msg);
                YYERROR;
            } else {
                //The return value is at (-2 - nparam), relative to FP
                emitOffset(OP_STORE, 1, -2 - scope->nparam);
                emit(OP_RTS);
            }
        }
     | IDENT LPAREN 
       {
            emitTwo(OP_LOADI, 0);
       } opt_arg_list RPAREN 
        { 
            if(!callFunc($1, $4)) { YYERROR; } 
            emitTwo(OP_POP, 1);
        } 
     | comp_stmt
     ;


elsepart : ELSE { $$ = emitTwo(OP_JUMP, 0); }

whilepart : WHILE { $$ = getAddr(); }

/* We don't need to do anything special with arguments except count them */
opt_arg_list : expr arg_list { $$ = $2 + 1;}
               | /* empty */ { $$ = 0; }
               ;

arg_list : COMMA expr arg_list{ $$ = $3 + 1; }
           | /* empty */ { $$ = 0; }
           ;

comp : expr EQ expr
        {
            //We emit two jumps: The true case jumps to immediately after the 
            //comp, while the false case needs to be backpatched later (to the
            //first address after the 'true' codeblock
            emit(OP_SUB);
            emit(OP_TEST);
            emitTwo(OP_POP, 1);
            //getAddr()+4 takes us over the JLT and the JUMP
            emitTwo(OP_JEQ, getAddr()+4);
            $$ = emitTwo(OP_JUMP, 0);
        }
     | expr LT expr
        {
            emit(OP_SUB);
            emit(OP_TEST);
            emitTwo(OP_POP, 1);
            emitTwo(OP_JLT, getAddr()+4);
            $$ = emitTwo(OP_JUMP, 0);
        }
     ;

/* EXPRESSIONS */

expr : expr PLUS term { emit(OP_ADD); }
     | expr MINUS term { emit(OP_SUB); }
     | term

term : term MULT fact { emit(OP_MUL); }
     | fact

fact : MINUS fact { emit(OP_NEG); }
     | LPAREN expr RPAREN 
     | NUM { 
            int val = $1;
            if(val >= 0 && val < 32768) {
                emitTwo(OP_LOADI, val);
            }
            else {
                char msg[50];
                sprintf(msg, "Numeric literal out of range: %d", val);
                yyerror(msg);
                YYERROR;
            }
        }
     | IDENT 
        {
            sym_var *v = getVar($1, scope);
            if(v == NULL) {
                errorUndefinedVar($1);
                YYERROR;
            }
            else {
                emitOffset(OP_LOAD, v->scope != NULL ? 1 : 0, v->addr);
            }
        }
     | IDENT LPAREN 
       {
            //Emit a space for the return value before we do anything with arguments
            emitTwo(OP_LOADI, 0);
       } opt_arg_list RPAREN 
        { 
            if(!callFunc($1, $4)) { YYERROR; } 
            //The function call pops all of the params from the stack, meaning that all that is left on there is the return value 
        }

%%

void yyerror(const char *s) {
    printf("Error: %s on line %d\n", s, 
            yylloc.first_line);
}

int callFunc(char *name, int numArgs) {
    sym_func *f = getFunc(name);
    if(f) {
        if(f->nparam == numArgs) {
            emitTwo(OP_JSR, f->address);
            emitTwo(OP_POP, f->nparam); 
            return 1;
        } else {
            char msg[100];
            sprintf(msg, "Expected %d parameters for function %s, received %d",
                    f->nparam, name, numArgs);
            yyerror(msg);
        }
    } else {
        //If we do not recognise the function, add it to a table to deal with later
        func_call call;
        call.nparam = numArgs;
        call.name = name;
        call.line = yylloc.first_line;
        call.jumploc = emitTwo(OP_JSR, 0); //We'll backpatch this later
        emitTwo(OP_POP, numArgs);

        callTable[fcallindex] = call;
        fcallindex++;
        return 1;
    }
    return 0;
}

void errorUndefinedVar(char *ident) {
    char msg[100];
    sprintf(msg, "Reference to undefined variable: %s", ident);
    yyerror(msg);
}
