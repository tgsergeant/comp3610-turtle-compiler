/*
 * Codegen.c - Code generation tools for turtle compiler
 * Tim Sergeant (u5006960), 2013
 */
#include "codegen.h"
#include <stdio.h>

unsigned short codeBuffer[65536];
addr currAddress = 0;

addr getAddr() {
    return currAddress;
}

addr emit(unsigned short instruction) {
    codeBuffer[currAddress] = instruction;
    return currAddress++; //Post-increment means we return the original address
}

addr emitOffset(unsigned short instruction, int fp, signed char offset) {
    if(fp) {
	instruction = instruction | 0x0100;
    }
    instruction = instruction | ((unsigned char)offset);
    return emit(instruction);
}

addr emitTwo(unsigned short instruction, unsigned short argument) {
    addr start = emit(instruction);
    emit(argument);
    return start;
}

void backpatch(addr address, addr jumpAddress) {
    codeBuffer[address+1] = jumpAddress;
}

void dumpCode(char *filename) {
    FILE *f = fopen(filename, "w");
    int i;
    for (i = 0; i < currAddress; i++) {
	fprintf(f, "%d\n", codeBuffer[i]);
    }
    fclose(f);
}
