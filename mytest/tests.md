Turtle test programs
===

This file describes each of the tests in roughly the order they were developed.
These test files are designed to cover all of the possible error messages, as
well as all of the statements and expressions in the turtle language. Compiled
and disassembled versions of all of these tests have also been included.

* basic.t: Test drawing functions without any advanced expressions, variables or
  functions

* functions.t: Test basic function definitions and errors surrounding function
  calls and definitions

* funcall.t: Test calling a function with variable arguments

* numbers.t: Test large numbers, unary negation and precedence

* load.t: Test local variables in functions vs global variables.

* funvar.t: Test using local variables vs function arguments 

* vars.t: Testing variable assignment as well as global vs local scope

* returns.t: Testing return statements in expressions and statements

* if.t: Testing hanging-else problem

* while.t: Testing that while statements work correctly 

* read.t: Test the read statement

* implicit.t: Testing implicit function declarations 


