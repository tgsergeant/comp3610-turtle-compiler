turtle funvar
fun box(x, y, w, h)
{
	up
	moveto(x, y)
	down
	moveto(x+w, y)
	moveto(x+w, y+h)
	moveto(x, y+h)
	moveto(x, y)
	up
}

fun b()
var x = 100
var y = 100
var w = 200
var h = 50
{
	box(x, y, w, h)
}

{
	b()
}
