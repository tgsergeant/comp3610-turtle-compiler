//Testing whether hanging else statements work correctly
//In this case, a value of 3 or 4 will result in a small
//box and a value of 5 or more will not draw anything
turtle iffy
var x = 7

fun box(x, y, w, h)
{
	up
	moveto(x, y)
	down
	moveto(x+w, y)
	moveto(x+w, y+h)
	moveto(x, y+h)
	moveto(x, y)
	up
}
{
	if(x < 5) 
		if(x < 2)
			box(200, 200, 200, 200)
		else
			box(100, 100, 100, 100)
}
