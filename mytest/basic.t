// Basic turtle to test drawing functions without any
// advanced expressions, variables or functions
turtle basic
{
	up
	moveto(100, 100)
	down
	moveto(100, 200)
	moveto(200, 200)
	moveto(200, 100)
	moveto(100, 100)
}
