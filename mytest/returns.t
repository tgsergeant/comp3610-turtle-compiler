//Testing the return statement, and what happens to the returned
//value when it is called from a statement vs an expression
turtle returns

fun ret(x, y, z)
{
	return 100
}

{
	up
	moveto(50, 50)
	down
	moveto(ret(1, 2, 3), ret(3, 2, 1))
	ret(5, 4, 3)
}
