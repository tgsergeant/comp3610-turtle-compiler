//Testing implicit declarations of functions
turtle implicitdec

fun box(x, y, w, h)
{
	up
	moveto(x, y)
	down
	moveto(x+w, y)
	moveto(x+w, y+h)
	moveto(x, y+h)
	moveto(x, y)
	up
}

fun main()
{
blee(300, 200)
//Test using the wrong number of arguments
//blee(300, 200, 100)

//Test calling a function that doesn't exist
//bleep(300, 200)
}

fun blee(x, y)
{
box100(x, y)	
}

fun box100(x, y)
{
box(x, y, 100, 100)
}

{
main()
}
