//Test calling a function with arguments
turtle func
var x_ = 100
var y_ = 300
var width = 50
var height = 200

fun box(x, y, w, h)
{
  up
  moveto(x, y)
  down
  moveto(x+w, y)
  moveto(x+w, y+h)
  moveto(x, y+h)
  moveto(x, y)
  up
}
{
  box(x_, y_, width, height)
}
