//Test errors relating to function definitions

turtle functions

fun hello(a, b, c, d)
{
    up
}

//Test multiple declarations of functions (not allowed)
//fun hello(a, b)
//{
    //down
//}

{
    //Test calling with the wrong number of arguments
    //hello(1, 2, 3)

    hello(1, 2, 3, 4)
    
    //Test calling a function which does not exist
    //helloooo(1, 2)
}
