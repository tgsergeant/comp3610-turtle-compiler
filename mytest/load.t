//Test variables in local vs global scope
turtle loader
var x
var y = 3
var z = 10 * 3 + 4

fun thing(yy)
var x = 3
var y = 2
var hello = 3 * 1
{
   up
   moveto(x, y)
}

{
   down
   moveto(x, y)
}
