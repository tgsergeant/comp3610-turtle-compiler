//Test of while statement
turtle whilt
var i = 0
var max = 5

fun box(x, y, w, h)
{
	up
	moveto(x, y)
	down
	moveto(x+w, y)
	moveto(x+w, y+h)
	moveto(x, y+h)
	moveto(x, y)
	up
}

{
	while(i < max)
	{
		box(i*100, i*100, i*30, i*30)
		i = i + 1
	}
}
