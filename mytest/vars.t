//Testing to make sure that variables are added to the right
//place in the symbol table in global and local scope.
//Intended to be run with verbose option to inspect the symbol table
turtle vars
var x
var y
var z

fun hello(a, b, c)
var x = 15
{
	return (a + b + c) * 20
}
{
	x = 20
	y = 30
	z = 40
	//Testing assignment to a variable that doesn't exist
	//hi = 50
	hello(x, y, z)
	//Testing var that doesn't exist in expression
	//moveto(hi, hello)
}
